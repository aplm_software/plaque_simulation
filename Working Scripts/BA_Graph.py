import numpy as np


class vertex():

	def __init__(self,vertex_number,vertex_links):
		self.vertex_number=vertex_number
		self.vertex_links=vertex_links

	def append_vertex_link(self,vertex_link):
		self.vertex_links.append(vertex_link)
		return self

	def return_number_of_vertex_links(self):
		return len(self.vertex_links)

	def print_vertex(self):
		print('vertex number is: ' + str(self.vertex_number))
		print('vertex links are: ' + str(self.vertex_links))



class bag():
	def __init__(self,number_of_nodes,m_number):
		self.m_number=m_number
		self.vertices=[]
		#must initialise
		self.vertices.append(vertex(len(self.vertices),[1]))
		self.vertices.append(vertex(len(self.vertices),[0]))
		self.number_of_vertices=len(self.vertices)
		#now grow the network
		while len(self.vertices)<number_of_nodes:
			self.add_vertex_with_links()
			if len(self.vertices)%100==0:
				print(str(len(self.vertices)) + ' completed.')
	def return_degree_list(self): #GETTER
		return [vertex.return_number_of_vertex_links() for vertex in self.vertices]
	def return_total_number_of_edges(self): #GETTER
		#returns double number of edges as counts each edge twice
		degree_list=self.return_degree_list()
		return(sum(degree_list))
	def return_individual_probability_list(self): #GETTER
		k=self.return_degree_list()
		return [float(d)/self.return_total_number_of_edges() for d in self.return_degree_list()]
	def return_cumulative_probability_list(self): #GETTER
		cumulative_list=[]
		sequential_list=self.return_individual_probability_list()
		for i,indiv_p in enumerate(sequential_list):
			cumulative_list.append(sum(sequential_list[0:i+1]))
		return cumulative_list
	def choose_vertices(self): #GETTER 
		chosen_vertices=[]
		cumulative_list=self.return_cumulative_probability_list()
		while len(chosen_vertices)<self.m_number:
			interval_sample=np.random.rand()
			current_vertex_chosen=False
			for i,cumul_prob in enumerate(self.return_cumulative_probability_list()):
				if interval_sample<cumul_prob and i not in chosen_vertices and current_vertex_chosen==False:
					chosen_vertices.append(i)
					current_vertex_chosen=True
		return chosen_vertices
	def add_vertex_with_links(self):
		#for one timestep
		chosen_vertices=self.choose_vertices()
		new_vertex_index=len(self.vertices)
		#first update the connections in the relevant vertices
		for chosen_vertex_index in chosen_vertices:
			self.vertices[chosen_vertex_index].append_vertex_link(new_vertex_index)
		#now create the new vertex
		self.vertices.append(vertex(len(self.vertices),chosen_vertices))
		return self

	def return_adjacency_matrix(self):
		adjacency_matrix=np.zeros(shape=(len(self.vertices),len(self.vertices)))
		#add diagonal 1 including for self as we will use for markov chain
		for v in self.vertices:
			v_i=v.vertex_number
			edges=v.vertex_links
			all_adjacent=[v_i] + edges
			for l_i in all_adjacent:
				adjacency_matrix[v_i,l_i]=1
		return adjacency_matrix