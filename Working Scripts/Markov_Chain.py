from reqs import *

class markov_chain():

    def __init__(self,transition_matrix,degree_vector):
        self.transition_matrix=transition_matrix
        self.dims = transition_matrix.shape
        self.degree_vector=degree_vector

        self.result_names=[]
        self.result_degrees=[]
        self.wp=self.dims[0]

        self.randomly_initialise_state()

    def set_state_vector(self,state_vector):
        self.state_vector=state_vector

        return self

    def get_next_state(self):
        
        weight_sum = sum(self.sps)
        weighted_state = map(lambda e: e * self.wp / weight_sum, self.sps)

        state_list = []
        i = 0
        for item in weighted_state:
            state_list += [ i ] * int(item) 
            i += 1
        new_state = [0.] * len(self.sps)

        #update res and deg...

        choice=random.choice(state_list)
        self.result_names.append('S'+str(choice))
        self.result_degrees.append(self.degree_vector[choice])

        new_state[choice] = 1.0;
         
        self.state_vector=new_state
        return self     

    def simulate(self):
        self.sps=np.dot(self.state_vector,self.transition_matrix)
        self.get_next_state()
        #update the ones...

        return self

    def simulate_for_n_steps(self,n_steps):
        #simulates for n steps then returns the vector of vertex names and degrees

        for k in range(n_steps):
            self.simulate()
            if (k+1)%1000==0:
               print(str(k+1) + ' simulations done!')            

        return self.result_degrees,self.result_names

    def randomly_initialise_state(self):

        # create state_vector
        self.state_vector = np.zeros(shape=self.dims[0])
        
        #set state vector nonzero index.
        sv_nz=np.random.randint(0,self.dims[0])
        self.state_vector[sv_nz]=1.0

        return self

    def reset_history(self):
        self.result_names=[]
        self.result_degrees=[]
        return self        

