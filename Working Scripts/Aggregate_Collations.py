#average deg calc

from reqs import * 
list_of_fn=os.listdir('results/')

list_of_names_fn=[fn for fn in list_of_fn if 'result_names' in fn]
list_of_degrees_fn=[fn for fn in list_of_fn if 'result_degrees' in fn]



for k in range(len(list_of_names_fn)):
	print(str(list_of_names_fn[k]) + ' : ' + str(list_of_degrees_fn[k])) 

#need to order them...

ordered_list_of_names_fn=[]
ordered_list_of_degrees_fn=[]

compatible_sequence_indices=[]

for k in range(len(list_of_names_fn)):
	print(k)
	req_name_fn=[name_fn for name_fn in list_of_names_fn if '_' + str(k) + '.txt' in name_fn]
	req_deg_fn=[deg_fn for deg_fn in list_of_degrees_fn if '_' + str(k) + '.txt' in deg_fn]

	if len(req_name_fn)==1 and len(req_deg_fn)==1:
		ordered_list_of_names_fn.append(req_name_fn[0])
		ordered_list_of_degrees_fn.append(req_deg_fn[0])	
		compatible_sequence_indices.append(k)	

for k in range(len(ordered_list_of_names_fn)):
	print(str(ordered_list_of_names_fn[k]) + ' : ' + str(ordered_list_of_degrees_fn[k])) 



#now we have ordered them.


#run through each ordered pair :)


for jjj in range(len(ordered_list_of_names_fn)):

	NAMES_FN='results/' +ordered_list_of_names_fn[jjj]
	DEGREES_FN='results/' + ordered_list_of_degrees_fn[jjj]
	TRIAL_INDEX=compatible_sequence_indices[jjj]


	names=np.loadtxt(NAMES_FN,dtype='str')
	degrees=np.loadtxt(DEGREES_FN,dtype='int32')

	indiv_degrees=set(list(degrees))


	all_cumulative_degrees=[]
	indiv_names=set(list(names))
	indiv_names_number=[i[1:] for i in indiv_names]

	all_cumulative_names=[]

	for k in range(0,len(indiv_names)):
		target_name='S' + str(k)
		num_incident=0
		for occurrence in names:
			if occurrence==target_name:
				num_incident+=1
		all_cumulative_names.append(num_incident)
	num_in_degree=[0]*(max(degrees)+1)



	#get the number of vertices corresponding to each degree
	for k in range(0,len(indiv_names)):
		score_for_name=all_cumulative_names[k]

		target_name='S' + str(k)

		for i, occurrence in enumerate(names):
			if occurrence==target_name:
				target_degree=degrees[i]

		num_in_degree[target_degree]+=1



	summary_degree_totals=[0]*(max(degrees)+1)

	#get the total corresponding to each degree
	for k in range(0,len(indiv_names)):
		score_for_name=all_cumulative_names[k]

		target_name='S' + str(k)

		for i, occurrence in enumerate(names):
			if occurrence==target_name:
				target_degree=degrees[i]

		summary_degree_totals[target_degree]+=score_for_name

	degree_averages=[0]*(max(degrees)+1)
	#create degree averages:

	for deg in indiv_degrees:
		degree_averages[deg]=float(summary_degree_totals[deg])/float(num_in_degree[deg])

	print(degree_averages) #save this one

	#form output name

	OUT_NAME='results/degree_averages_' + str(TRIAL_INDEX) + '.txt'

	np.savetxt(OUT_NAME,degree_averages)

	degree_averages_summary=[degree_averages[deg] for deg in indiv_degrees]

	print(str(jjj+1) + ' done of ' + str(len(compatible_sequence_indices)) + '!')

	





