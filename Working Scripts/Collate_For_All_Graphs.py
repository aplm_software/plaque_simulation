#Collate for all graphs
from reqs import * 

def format_visits_string(ind):
	out_str='results/BA_Graph_'+str(ind)+'/sum_visits_dict_'+str(ind)+'.txt'
	return out_str

def format_vertex_string(ind):
	out_str='results/BA_Graph_'+str(ind)+'/sum_vertex_dict_'+str(ind)+'.txt'
	return out_str

def Collate_For_All_Graphs():

	all_graph_fn=[fn for fn in os.listdir('results/') if 'BA_Graph_' in fn]
	graph_indices = [int(re.sub('\D', '', ag)) for ag in all_graph_fn]

	#dictionary directories now...
	vert_dict_dir=[format_vertex_string(ind) for ind in graph_indices]
	vist_dict_dir=[format_visits_string(ind) for ind in graph_indices]

	print(vert_dict_dir)

	#separate by vertex and visits 
	vert_dicts = []
	vist_dicts = []

	max_dict_len=0
	for vert_dict_d,vist_dict_d in zip(vert_dict_dir,vist_dict_dir):
		
		with open(vert_dict_d, 'rb') as in_fn:
			in_dict = cPickle.load(in_fn)
			vals = [in_dict[k] for k in in_dict.keys()]
			vert_dicts.append(vals)
		
		with open(vist_dict_d, 'rb') as in_fn:
			in_dict = cPickle.load(in_fn)
			vals = [in_dict[k] for k in in_dict.keys()]
			vist_dicts.append(vals)

	mdl=max([len(v_d) for v_d in vert_dicts])

	#pad w zero if necessary
	for k,v_d in enumerate(vert_dicts):
		v_dif=mdl-len(v_d)
		if v_dif>0:
			vert_dicts[k]+=[0]*v_dif
			vist_dicts[k]+=[0]*v_dif


	vert_stack = np.vstack(vert_dicts)
	vert_sum = vert_stack.sum(axis=0)

	vert_sum_dict={d:vert_sum[d] for d in range(mdl)}

	vist_stack = np.vstack(vist_dicts)
	vist_sum = vist_stack.sum(axis=0)

	vist_sum_dict={d:vist_sum[d] for d in range(mdl)}
	
	if 'All_Results' not in os.listdir('results/'):
		os.mkdir('results/All_Results/')

	#save
	vert_sum_dict_fn = 'results/All_Results/total_vert.txt'
	vist_sum_dict_fn = 'results/All_Results/total_vist.txt'

	with open(vert_sum_dict_fn,'wb') as f:
		cPickle.dump(vert_sum_dict,f)

	with open(vist_sum_dict_fn,'wb') as f:
		cPickle.dump(vist_sum_dict,f)




