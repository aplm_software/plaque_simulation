
from reqs import *

def Simulate_BA_Graph(NUM_SIMULATIONS,NUM_PARTICLES,graph_no):

	save_dir='results/BA_Graph_' + str(graph_no) + '/'
	fn='BA_Graph_' + str(graph_no) + '.txt'

	#load
	with open(save_dir+fn,'rb') as fp:
		cur_BA=cPickle.load(fp)

	#we need adjacency matrix for markov / ergodic simulation
	cur_BA_adjacency=cur_BA.return_adjacency_matrix()
	transition_matrix=cur_BA.return_transition_matrix()

	#get trans matrix dimensions
	dims = transition_matrix.shape

	#measure the degree of each vertex
	degree_vector = np.zeros(shape=dims[0])

	#set degree vector here
	for row in range(dims[0]):
	    cur_d=0
	    for col in range(dims[1]):
	        if transition_matrix[row,col]>0:
	            cur_d+=1
	    degree_vector[row]=cur_d

	#now create sim object
	markov_sim = markov_chain(transition_matrix,degree_vector)

	#now simulate
	for k in range(NUM_PARTICLES):
		markov_sim.randomly_initialise_state()
		result_degrees, result_names = markov_sim.simulate_for_n_steps(NUM_SIMULATIONS)

		#save
		out_degs_fn = save_dir + 'result_degrees_' + str(k) + '.txt'
		out_names_fn = save_dir + 'result_names_' + str(k) + '.txt'
		np.savetxt(out_degs_fn, result_degrees,delimiter=' ')
		np.savetxt(out_names_fn, result_names,delimiter=' ',fmt="%s")

		#and clear

		markov_sim.reset_history()
		

		print(str(k+1) + ' of total: ' + str(NUM_PARTICLES) + ' particles have been simulated.')



