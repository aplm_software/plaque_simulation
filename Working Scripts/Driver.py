#Driver class

#IMPORTS
from reqs import *
from Generate_BA_Graph import *
from Simulate_BA_Graph import *
from Collate_Simulation_Aggregate import *
from Collate_For_Graph import *

#RANDOM_SEED
RANDOM_SEED=1000

#GRAPH_SIZE
GRAPH_SIZE=1000

#NUMBER_OF_GRAPHS
NUMBER_OF_GRAPHS=6

#SET RANDOM_SEED
np.random.seed(RANDOM_SEED)

#GENERATE GRAPHS
def generate_graphs():
	
	print('Now generating all: ' + str(NUMBER_OF_GRAPHS) + ' graphs ^_^')
	for graph_no in range(NUMBER_OF_GRAPHS):
		Generate_BA_Graph(graph_no,GRAPH_SIZE)
		print('Graph: ' + str(graph_no+1) + ' generated.')

#SIMULATE GRAPHS
NUM_PARTICLES=100
NUM_SIMULATIONS=100

def simulate_graphs():

	print('Now simulating all: ' + str(NUMBER_OF_GRAPHS) + ' graphs ^_^')
	for graph_no in range(NUMBER_OF_GRAPHS):
		Simulate_BA_Graph(NUM_SIMULATIONS,NUM_PARTICLES,graph_no)
		print('Graph: ' + str(graph_no+1) + ' simulated.')


#COLLATE RESULTS FROM SIMULATIONS
def collate_results():
	for graph_no in range(NUMBER_OF_GRAPHS):
		Collate_Simulation_Aggregate(graph_no)

#AGGREGATE ALL RESULTS FROM GRAPH
def collate_graph_results():
	for graph_no in range(NUMBER_OF_GRAPHS):
		Collate_For_Graph(graph_no)


#AGGREGATE ALL RESULTS FOR ALL GRAPHS
def collate_for_all_graphs():
	Collate_For_All_Graphs()


