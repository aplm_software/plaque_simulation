#average deg calc

from reqs import * 

def Collate_For_Graph(graph_no):
	save_dir='results/BA_Graph_' + str(graph_no) + '/'

	list_of_fn=os.listdir(save_dir)

	#visits
	#number of nodes of certain degree


	#load all dicts for each case

	all_vertex_list = [fn for fn in os.listdir(save_dir) if 'num_in_degree_' in fn]

	all_visits_list = [fn for fn in os.listdir(save_dir) if 'summary_degree_totals_' in fn]

	all_visits=[np.loadtxt(save_dir + fn) for fn in all_visits_list]
	all_vertex=[np.loadtxt(save_dir + fn) for fn in all_vertex_list]


	max_len = max([len(a_v) for a_v in all_visits])

	for k,a_v in enumerate(all_visits):
		len_dif = max_len - len(a_v)
		if len_dif>0:

			dif_vec=[0]*len_dif

			all_visits[k]=np.concatenate((a_v,dif_vec),axis=0)
			
	for k,a_v in enumerate(all_vertex):
		len_dif = max_len - len(a_v)
		if len_dif>0:

			dif_vec=[0]*len_dif

			all_vertex[k]=np.concatenate((a_v,dif_vec),axis=0)
	



	visit_stack = np.vstack(all_visits)




	vertex_stack= np.vstack(all_vertex)



	visit_sum = visit_stack.sum(axis=0)
	vertex_sum= vertex_stack.sum(axis=0)
	
	#form two new dictionaries corresponding to number of visits and number of vertices of certain degree

	visits_dict={d:visit_sum[d] for d in range(max_len)}
	vertex_dict={d:vertex_sum[d] for d in range(max_len)}

	#save out



	out_visit_fn = save_dir + 'sum_visits_dict_' + str(graph_no) + '.txt'
	out_vertex_fn = save_dir + 'sum_vertex_dict_' + str(graph_no) + '.txt'

	with open(out_visit_fn,'wb') as fp:
		cPickle.dump(visits_dict,fp)


	with open(out_vertex_fn,'wb') as fp:
		cPickle.dump(vertex_dict,fp)

	print('Completed collating results for graph: ' + str(graph_no) + ' !')

