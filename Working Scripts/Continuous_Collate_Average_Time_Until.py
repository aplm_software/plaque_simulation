range_of_tests=[2,3,4,5,6,7,8,9,10]




#MULTIPLE EXPOSURES CALCULATOR (different)
from reqs import *


#format the out_string for saving in correct directory
def return_save_fn(data_type,directory,num_particles,threshold):
	#NB. NOT '.txt' format
	out_str = results_dir + data_type +'_part_' + str(num_particles) + '_thresh_' + str(threshold)

	return out_str
#maybe use pickle to save dictionary of results?

def pickle_save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj,f)

def pickle_load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f)

def zero_to_nan(values):
    """Replace every 0 with 'nan' and return a copy."""
    return [float('nan') if x==0 else x for x in values]


#load in results for each condition

ROOT_FOLDER='CONTINUOUS_CALCULATIONS'



#first you must find the appropriate file name for the relevant num_particles and threshold

#for all BA_GRAPH_

all_folders = ['results/' + fn + '/' for fn in os.listdir('results') if 'BA_Graph_' in fn]

separate_files = [os.listdir(k + ROOT_FOLDER) for k in all_folders]

col_res_dir='results/CONTINUOUS_CALCULATIONS/'

if 'CONTINUOUS_CALCULATIONS' not in os.listdir('results/'):
	os.mkdir(col_res_dir)

print(all_folders)

#print(separate_files)

'perc_occur_deg_part_X_thresh_Z.pkl' #percent occur per degree
'time_until_occur_deg_part_X_thresh_Z.pkl'	#average time until occur per degree
'avg_occur_deg_part_X_thresh_Z.pkl' #average number of occurrences

time_until_str = 'time_until_occur_part_'
perc_occur_str = 'perc_occur_deg_part_'
avg_occur_str = 'avg_occur_deg_part_'

thresh_str='thresh_'
fn_str='.pkl'




#get all directories....
file_names=[]
for folder in all_folders:
	files_in_dir = os.listdir(folder + ROOT_FOLDER)
	for f in files_in_dir:
		file_names.append(folder + ROOT_FOLDER + '/'+ f)

for num_particles in range_of_tests:
	for threshold in range_of_tests:
		if threshold>num_particles:
			dummy=1
		else:
			search_string = time_until_str + str(num_particles) + '_thresh_' + str(threshold) + '.pkl'
			sub_files = [sf for sf in file_names if search_string in sf]
			#load
			#and retrieve keys
			dictionaries=[]
			for f in sub_files:
				try:
					dictionaries.append(pickle_load_obj(f))
				except:
					print('failed to load at least one dictionary')
				
			keys=[]
			for dic in dictionaries:
				for key in dic:
					keys.append(key)
			unique_keys=list(set(keys))
			#and now we iterate through all dictionaries and look for the key...
			avg_dict={} #set baseline empty dict for storage purposes.
			for u_k in unique_keys:
				sub_uk=[]
				for dic in dictionaries:
					if u_k in dic:
						sub_uk.append(dic[u_k])
				#print('u_k being: ' + str(u_k) + ' gives: ')
				#print(sub_uk)
				#replace 'never_happens'
				filt_suk=[] 
				for sub in sub_uk:
					if sub!='NEVER_HAPPENS':
						filt_suk.append(float(sub))
					else:
						filt_suk.append(0)
				#and then produce a nice average
				if len(filt_suk)>0:
					avg = sum(filt_suk)/len(filt_suk)

					avg_dict[u_k]=avg

			#save out as a results.txt file...
			results=np.array([],dtype='float')
			max_deg=max(unique_keys)
			for deg in range(max_deg):
				if deg in avg_dict:
					results = np.append(results,avg_dict[deg])
				else:
					results = np.append(results,0)
			print(results)	
			#save out results....
			fname=col_res_dir + 'avg_time_until_part_' + str(num_particles) + '_thresh_' + str(threshold) + '.txt'
			np.savetxt(fname,results)
#now produce plots and save them all....
#need nice directory

#'plt'

for fn in os.listdir(col_res_dir):

	if '.png' in fn or '.txt' not in fn:
		#skip
		skipping='skipping'
	else:

		target_f=col_res_dir + fn

		plt = None

		import matplotlib.pyplot as plt

		data = np.loadtxt(target_f)

		plt.plot(data,'ro')


		save_f=col_res_dir +'plt/' +  fn
		plt.savefig(save_f[:-4], bbox_inches='tight')

		plt.clf()

#try removing the zero vals

#set to nan...


for fn in os.listdir(col_res_dir):

	if '.png' in fn or '.txt' not in fn:
		#skip
		skipping='skipping'
	else:
		target_f=col_res_dir + fn
		plt = None
		import matplotlib.pyplot as plt
		data = np.loadtxt(target_f)
		data = zero_to_nan(data)
		plt.plot(data,'ro')
		save_f=col_res_dir +'plt_zero_rem/' +  fn
		plt.savefig(save_f[:-4] + 'zero_rem.png', bbox_inches='tight')
		plt.clf()

#and then plot one on top of another, changing colours for each also :)

part_strings=['part_' + str(rot) for rot in range_of_tests]

colours=['darkorange',
'limegreen',
'black',
'gray',
'brown',
'red',
'greenyellow',
'olive',
'goldenrod',
'mediumturquoise',
'lightskyblue',
'mediumblue',
'fuchsia',
'indigo'
]

for ps in part_strings:
	import matplotlib.pyplot as plt


	col_index=0
	

	for fn in os.listdir(col_res_dir):

		if '.png' in fn or '.txt' not in fn or ps not in fn or 'avg_time_' not in fn:
			#skip
			skipping='skipping'
		else:
			target_f=col_res_dir + fn
			
			data = np.loadtxt(target_f)
			data = zero_to_nan(data)

			#also we need to get proper label

			lab_sub_loc = target_f.find('thresh_')
			lab_sub_right=target_f.find('.txt')

			thresh_ind=target_f[lab_sub_loc+7:lab_sub_right]

			if list(set(data))==[float('nan')]:
				skipping='skipping'

			else:

				plt.plot(data,'ro',color=colours[col_index],label='Threshold: ' + str(thresh_ind))



			col_index+=1
			print('col_index: ' + str(col_index))

	save_f=col_res_dir +'plt_overlay/'

	plt.legend()
	#plt.collate()
	plt.savefig(save_f + 'avg_' + ps + '_overlay.png', bbox_inches='tight')
	plt.clf()
	plt.cla()
	plt.close()

