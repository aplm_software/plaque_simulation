

#MULTIPLE EXPOSURES CALCULATOR (different)
from reqs import *

graph_indices=[int(fn[9:]) for fn in os.listdir('results') if 'BA_Graph_' in fn]


for BA_GRAPH_NUMBER in graph_indices:
	working_directory='results/BA_Graph_' + str(BA_GRAPH_NUMBER) + '/'

	list_of_fn=os.listdir(working_directory)

	list_of_names_fn=[fn for fn in list_of_fn if 'result_names' in fn]
	list_of_degrees_fn=[fn for fn in list_of_fn if 'result_degrees' in fn]

	#make result directory...

	MEAN_RES_DIR='CONTINUOUS_CALCULATIONS'

	if MEAN_RES_DIR not in list_of_fn:
		os.mkdir(working_directory + MEAN_RES_DIR)

	results_dir=working_directory + MEAN_RES_DIR + '/'


	#nuke the directory to start over
	for f in os.listdir(results_dir):
		os.remove(results_dir + f)

	#need to order them...

	ordered_list_of_names_fn=[]
	ordered_list_of_degrees_fn=[]

	compatible_sequence_indices=[]

	for k in range(len(list_of_names_fn)):
		req_name_fn=[name_fn for name_fn in list_of_names_fn if '_' + str(k) + '.txt' in name_fn]
		req_deg_fn=[deg_fn for deg_fn in list_of_degrees_fn if '_' + str(k) + '.txt' in deg_fn]

		if len(req_name_fn)==1 and len(req_deg_fn)==1:
			ordered_list_of_names_fn.append(req_name_fn[0])
			ordered_list_of_degrees_fn.append(req_deg_fn[0])	
			compatible_sequence_indices.append(k)	

	#now we have ordered them.


	#run through each ordered pair
	#CREATE DICTIONARY

	degree_name_dict={}

	#1. Read in all names/degrees

	all_names=[]
	all_degrees=[]
	for fj in range(len(ordered_list_of_names_fn)):
		w_names_fn=working_directory + ordered_list_of_names_fn[fj]
		w_degrees_fn=working_directory + ordered_list_of_degrees_fn[fj]

		w_names=np.loadtxt(w_names_fn,dtype='str')
		w_degrees=np.loadtxt(w_degrees_fn,dtype='int32')

		all_names=all_names + list(w_names)
		all_degrees= all_degrees + list(w_degrees)




	#now run through all_names to find degrees...

	unique_names=list(set(all_names))
	unique_degrees=list(set(all_degrees))

	for name in unique_names:
		not_found=True
		k=0
		while not_found and k < len(all_names):
			if all_names[k]==name:
				dict_degree=all_degrees[k]
				degree_name_dict[name]=dict_degree
				not_found=False
			k+=1


	#dictionary now found..

	#save the dictionary!!
	degree_name_dict_str=return_save_fn('degree_name_dict',working_directory,0,0,results_dir)

	#find num of trials for it to work (eg, 10000 trials in this instance...)
	num_trials=len(list(np.loadtxt(working_directory + ordered_list_of_names_fn[1],dtype='str')))

	#select NUM_PARTICLES of the file names at random from the list of file names
	num_of_files=len(ordered_list_of_names_fn)


	for NUM_PARTICLES in [100]:

		for CONTINUOUS_EXPOSURE_THRESHOLD in [2,3,4,5,6,7,8,9,10]:

			if CONTINUOUS_EXPOSURE_THRESHOLD>NUM_PARTICLES:
				hi_jones='hellodrpie'
			else:
				print('Starting...Particles= ' + str(NUM_PARTICLES) + ', Threshold= ' + str(CONTINUOUS_EXPOSURE_THRESHOLD))
		#lets do NUM_PARTICLES in (2,4,7,10,15,20,25,30,35,)
		#and also let's do timestep exposure threshold in (2,4,5,10,15,20,25,30,35)



				#file index....
				selected_file_indices=[]
				for k in range(NUM_PARTICLES):
					sel_index=np.random.randint(num_of_files)
					selected_file_indices.append(sel_index)

				result_chain=np.empty((num_trials,NUM_PARTICLES),dtype="S10")


				for i,sel in enumerate(selected_file_indices):
					result_chain[:,i]=np.loadtxt(working_directory + ordered_list_of_names_fn[sel],dtype='str')

				#print(result_chain)


				nrow,ncol=result_chain.shape

				degree_incidences_dict={}

				ddegree_time_until_occur_dict={}
				ddegree_has_occurred_dict={}
				ddegree_individual_number_of_occurrence={}


				for u_deg in unique_degrees:

					ddegree_time_until_occur_dict[u_deg]=[]
					ddegree_has_occurred_dict[u_deg]=[]#T/F
					ddegree_individual_number_of_occurrence[u_deg]=[]

				candidate_str=['1']*CONTINUOUS_EXPOSURE_THRESHOLD

				#we want to measure how many continuous we get
				for name in unique_names:

					u_deg=degree_name_dict[name]

					binary_occurrence_vector=[0]*nrow
					
					for row in range(nrow):
						interest_name_list = list(result_chain[row,:])

						if interest_name_list.count(name)>0:
							binary_occurrence_vector[row]=1
							
					degree_incidences_dict[name]=binary_occurrence_vector

					#time until first
					cont_str=str(binary_occurrence_vector)
					islands=cont_str.split('0')
					islands_unique=list(islands)
					occurred=candidate_str in islands_unique
					if occurred:
						time_oc=cont_str.find(candidate_str) + CONTINUOUS_EXPOSURE_THRESHOLD-1
						degree_time_until_first_dict[name]=time_oc
						ddegree_time_until_occur_dict[u_deg].append(time_oc)
						ddegree_has_occurred_dict[u_deg].append(1)
						degree_has_occurred_dict[name]=True
						#total num incident
						islands_rel=[i for i in islands_unique if len(i)>=CONTINUOUS_EXPOSURE_THRESHOLD]
						#need to count continuous also..
						islands_rel_sub=[i[CONTINUOUS_EXPOSURE_THRESHOLD-1:] for i in islands_rel]
						big_comb=''.join(islands_rel_sub)
						degree_number_of_incident_overall_dict[name]=len(big_comb)
						
						ddegree_individual_number_of_occurrence[u_deg].append(len(big_comb))
					else:
						ddegree_time_until_occur_dict[name]='NOT_FOUND'
						ddegree_has_occurred_dict[u_deg].append(0)
						ddegree_has_occurred_dict[name]=False
						ddegree_individual_number_of_occurrence[name]=0

				#now return statistics for....

				#now analyse according to degree
				#1. percentage of occurrence as function of degree 
				#eg "50% of nodes of degree 3 witnessed this event at least once"
				#2. mean num occurrence per degree
				#3. mean time until first occurrence
				#eg. "for nodes of degree 32, time until first occurrence was t_0 timesteps"
				ddegree_time_until_occur_dict_avg={}
				ddegree_has_occurred_dict_avg={}
				ddegree_individual_number_of_occurrence_avg={}

				for u_deg in unique_degrees:
					ddegree_time_until_occur_dict_avg[u_deg]=np.mean(ddegree_time_until_occur_dict[u_deg])
					ddegree_has_occurred_dict_avg[u_deg]=np.mean(ddegree_has_occurred_dict[u_deg])
					ddegree_individual_number_of_occurrence_avg[u_deg]=np.mean(ddegree_individual_number_of_occurrence[u_deg])


				#										RESULT VECTORS									#			
				#=======================================================================================#
				# mean_time_until_occurrence_per_degree_dict	(avg time until it occurs)
				# average_occurrence_per_degree_dict			(number of occurrences)
				# cumulative_degree_has_occured_dict_avg		(it has or hasn't occurred as percentage)


				#										SAVINGS MY G									#			
				#=======================================================================================#

				#mean time until
				mtu_out_str=return_save_fn('time_until_occur',working_directory,NUM_PARTICLES,CONTINUOUS_EXPOSURE_THRESHOLD,results_dir)
				#average occurrence per degree
				avg_occur_str=return_save_fn('avg_occur_deg',working_directory,NUM_PARTICLES,CONTINUOUS_EXPOSURE_THRESHOLD,results_dir)
				#percentage_occurence(0,1) per degree
				perc_occur_str=return_save_fn('perc_occur_deg',working_directory,NUM_PARTICLES,CONTINUOUS_EXPOSURE_THRESHOLD,results_dir)



				pickle_save_obj(ddegree_time_until_occur_dict_avg,mtu_out_str)
				pickle_save_obj(ddegree_individual_number_of_occurrence_avg,avg_occur_str)
				pickle_save_obj(ddegree_has_occurred_dict_avg,perc_occur_str)

				
				print('Round Completed for: ' + str(BA_GRAPH_NUMBER))

			#save outputs...