#average deg calc

from reqs import * 

def Collate_Simulation_Aggregate(graph_no):
	save_dir='results/BA_Graph_' + str(graph_no) + '/'

	list_of_fn=os.listdir(save_dir)

	list_of_names_fn=[fn for fn in list_of_fn if 'result_names' in fn]
	list_of_degrees_fn=[fn for fn in list_of_fn if 'result_degrees' in fn]



	for k in range(len(list_of_names_fn)):
		print(str(list_of_names_fn[k]) + ' : ' + str(list_of_degrees_fn[k])) 

	#need to order them...

	ordered_list_of_names_fn=[]
	ordered_list_of_degrees_fn=[]
	compatible_sequence_indices=[]



	for k in range(len(list_of_names_fn)):
		print(k)
		req_name_fn=[name_fn for name_fn in list_of_names_fn if '_' + str(k) + '.txt' in name_fn]
		req_deg_fn=[deg_fn for deg_fn in list_of_degrees_fn if '_' + str(k) + '.txt' in deg_fn]

		if len(req_name_fn)==1 and len(req_deg_fn)==1:
			ordered_list_of_names_fn.append(req_name_fn[0])
			ordered_list_of_degrees_fn.append(req_deg_fn[0])	
			compatible_sequence_indices.append(k)	

	for k in range(len(ordered_list_of_names_fn)):
		print(str(ordered_list_of_names_fn[k]) + ' : ' + str(ordered_list_of_degrees_fn[k])) 





	#first wipe all the old ones ;)

	all_files = os.listdir(save_dir)
	print(all_files)

	#for fn in all_files:
	#	if 'num_in_degree' or 'summary_degree_totals' in fn:
	#		os.remove(save_dir + fn)



	for trial in range(len(ordered_list_of_names_fn)):

		NAMES_FN=save_dir +ordered_list_of_names_fn[trial]
		DEGREES_FN=save_dir + ordered_list_of_degrees_fn[trial]
		TRIAL_INDEX=compatible_sequence_indices[trial]


		names=np.loadtxt(NAMES_FN,dtype='str')
		degrees=np.loadtxt(DEGREES_FN,dtype='int32')

		indiv_degrees=set(list(degrees))


		all_cumulative_degrees=[]
		indiv_names=set(list(names))


		#1. Create Dictionary

		visit_dict = {name:(names==name).sum() for name in names}

		degree_dict = {name:-1 for name in names}
		#put in a safe check for degrees...
		for name in names:
			observed_name_degrees=degrees[np.where(names==name)]

			unique_nd=len(set(observed_name_degrees))
			if unique_nd==1:
				degree_dict[name]=observed_name_degrees[0]



		unique_degrees=list(set(degrees))

		unique_visit_dict = {u_d:names[np.where(degrees==u_d)] for u_d in unique_degrees}


		#want the length of each dict entry



		visits_dict = {key: len(value) for key, value in unique_visit_dict.items()}
		total_nodes_dict = {key: len(set(value)) for key, value in unique_visit_dict.items()}

		#then want the num unique in each dict entry


		max_deg=max(degrees)


		summary_degree_totals=[0]*(max(degrees)+1)

		num_in_degree=[0]*(max(degrees)+1)
		#num visits
		for u_d in unique_degrees:
			summary_degree_totals[u_d],num_in_degree[u_d]=visits_dict[u_d],total_nodes_dict[u_d]

		#so save out num_in_degree and summary_degree_totals


		OUT_NAME=save_dir + 'num_in_degree_' + str(TRIAL_INDEX) + '.txt'
		np.savetxt(OUT_NAME,num_in_degree)

		OUT_NAME=save_dir + 'summary_degree_totals_' + str(TRIAL_INDEX) + '.txt'
		np.savetxt(OUT_NAME,summary_degree_totals)


		print(str(trial+1) + ' done of ' + str(len(compatible_sequence_indices)) + '!')

