

#MULTIPLE EXPOSURES CALCULATOR (different)


import cPickle as pickle


#format the out_string for saving in correct directory
def return_save_fn(data_type,directory,num_particles,threshold,results_dir):
	#NB. NOT '.txt' format
	out_str = results_dir + data_type +'_part_' + str(num_particles) + '_thresh_' + str(threshold)

	return out_str
#maybe use pickle to save dictionary of results?

def pickle_save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj,f)

def pickle_load_obj(name ):
	if '.pkl' in name:
	    with open(name, 'rb') as f:
	        return pickle.load(f)		
	else:

	    with open(name + '.pkl', 'rb') as f:
	        return pickle.load(f)
