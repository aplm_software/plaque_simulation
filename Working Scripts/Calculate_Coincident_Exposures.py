

#MULTIPLE EXPOSURES CALCULATOR (different)

from reqs import *


graph_indices=[int(fn[9:]) for fn in os.listdir('results') if 'BA_Graph_' in fn]

for BA_GRAPH_NUMBER in graph_indices:

	working_directory='results/BA_Graph_' + str(BA_GRAPH_NUMBER) + '/'

	list_of_fn=os.listdir(working_directory)

	list_of_names_fn=[fn for fn in list_of_fn if 'result_names' in fn]
	list_of_degrees_fn=[fn for fn in list_of_fn if 'result_degrees' in fn]

	#make result directory...

	MEAN_RES_DIR='COINCIDENT_CALCULATIONS'

	if MEAN_RES_DIR not in list_of_fn:
		os.mkdir(working_directory + MEAN_RES_DIR)

	results_dir=working_directory + MEAN_RES_DIR + '/'

	#nuke the directory to start over
	for f in os.listdir(results_dir):
		os.remove(results_dir + f)

	ordered_list_of_names_fn=[]
	ordered_list_of_degrees_fn=[]

	compatible_sequence_indices=[]

	for k in range(len(list_of_names_fn)):
		req_name_fn=[name_fn for name_fn in list_of_names_fn if '_' + str(k) + '.txt' in name_fn]
		req_deg_fn=[deg_fn for deg_fn in list_of_degrees_fn if '_' + str(k) + '.txt' in deg_fn]

		if len(req_name_fn)==1 and len(req_deg_fn)==1:
			ordered_list_of_names_fn.append(req_name_fn[0])
			ordered_list_of_degrees_fn.append(req_deg_fn[0])	
			compatible_sequence_indices.append(k)	

	#CREATE DICTIONARY

	degree_name_dict={}

	#1. Read in all files in there...

	all_names=[]
	all_degrees=[]
	for fj in range(len(ordered_list_of_names_fn)):
		w_names_fn=working_directory + ordered_list_of_names_fn[fj]
		w_degrees_fn=working_directory + ordered_list_of_degrees_fn[fj]

		w_names=np.loadtxt(w_names_fn,dtype='str')
		w_degrees=np.loadtxt(w_degrees_fn,dtype='int32')

		all_names=all_names + list(w_names)
		all_degrees= all_degrees + list(w_degrees)

	#now run through all_names to find degrees...

	unique_names=list(set(all_names))
	unique_degrees=list(set(all_degrees))

	for name in unique_names:
		not_found=True
		k=0
		while not_found and k < len(all_names):
			if all_names[k]==name:
				dict_degree=all_degrees[k]
				degree_name_dict[name]=dict_degree
				not_found=False
			k+=1


	#dictionary now found..

	#save the dictionary!!
	degree_name_dict_str=return_save_fn('degree_name_dict',working_directory,0,0,results_dir)

	#find num of simulated steps
	num_trials=len(list(np.loadtxt(working_directory + ordered_list_of_names_fn[1],dtype='str')))

	#select NUM_PARTICLES of the file names at random from the list of file names
	num_of_files=len(ordered_list_of_names_fn)


	for NUM_PARTICLES in [100]:

		for COINCIDENT_EXPOSURE_THRESHOLD in [2,3,4,5,6,7,8,9,10]:

			if COINCIDENT_EXPOSURE_THRESHOLD>NUM_PARTICLES:
				print('threshold over available particles, skipping...')
			else:
				print('Starting...Particles= ' + str(NUM_PARTICLES) + ', Threshold= ' + str(COINCIDENT_EXPOSURE_THRESHOLD))

				#file index....
				selected_file_indices=[]
				for k in range(NUM_PARTICLES):
					sel_index=np.random.randint(num_of_files)
					selected_file_indices.append(sel_index)

				result_chain=np.empty((num_trials,NUM_PARTICLES),dtype="S10")

				for i,sel in enumerate(selected_file_indices):
					result_chain[:,i]=np.loadtxt(working_directory + ordered_list_of_names_fn[sel],dtype='str')

				nrow,ncol=result_chain.shape

				degree_incidences_dict={}

				#two metrics of interest...

				#1 = expected time until first
				#2 = mean number of incidences...

				#- go row-wise down the list, and if name is in any of them, put a '1' into array
				#- or if the name is not in, put a '0' into array
				#we want to measure how many coincident we get...so out of all in result_chain
				#count how many times we get 2,5,7,etc...

				for name in unique_names:
					binary_occurrence_vector=[]
					for row in range(nrow):
						interest_name_list = list(result_chain[row,:])
						num_coincident = interest_name_list.count(name)
						if num_coincident>=COINCIDENT_EXPOSURE_THRESHOLD:
							binary_occurrence_vector.append(1)
						else:
							binary_occurrence_vector.append(0)
					degree_incidences_dict[name]=binary_occurrence_vector

				degree_time_until_first_dict={}
				degree_has_occurred_dict={}

				#TIME UNTIL FIRST OCCUR
				for name in unique_names:
					binary_output_vec=degree_incidences_dict[name]
					found_first=False
					for k in range(len(binary_output_vec)):
							if binary_output_vec[k]==1:
								if found_first==False:
									incident_time=k
									found_first=True
									degree_time_until_first_dict[name]=incident_time
									k=len(binary_output_vec) #to exit loop
					if found_first==False:
						degree_time_until_first_dict[name]='NOT_FOUND'
						degree_has_occurred_dict[name]=False
					else:
						degree_has_occurred_dict[name]=True

				#total num of threshold incident
				degree_number_of_incident_overall_dict={}
				for name in unique_names:
					binary_output_vec=degree_incidences_dict[name]
					degree_number_of_incident_overall_dict[name]=sum(binary_output_vec)

				#now return statistics for....

				#1. occurrence/non-occurrence as a function of degree 
				#eg "50% of nodes of degree 3 witnessed this event at least once"
				#loop through degree names
				cumulative_degree_has_occured_dict={}

				for u_deg in unique_degrees:
					cumulative_degree_has_occured_dict[u_deg]=[]

				for name in unique_names:
					corresponding_degree=degree_name_dict[name]

					if degree_has_occurred_dict[name]==True:
						cumulative_degree_has_occured_dict[corresponding_degree].append(1.)
					else:
						cumulative_degree_has_occured_dict[corresponding_degree].append(0.)

				cumulative_degree_has_occured_dict_avg={}

				for udeg in unique_degrees:
					mean_occur=sum(cumulative_degree_has_occured_dict[udeg])/len(cumulative_degree_has_occured_dict[udeg])

					cumulative_degree_has_occured_dict_avg[u_deg]=mean_occur

				#2. average occurrence per degree
				#eg "for nodes of degree 5, on average each individual node saw this event 34 times"
				cumulative_occurrence_per_degree_dict={}

				for u_deg in unique_degrees:
					cumulative_occurrence_per_degree_dict[u_deg]=[]

				for name in unique_names:

					num_occur=degree_number_of_incident_overall_dict[name]
					corresponding_degree=degree_name_dict[name]

					cumulative_occurrence_per_degree_dict[corresponding_degree].append(num_occur)

				average_occurrence_per_degree_dict={}

				for udeg in unique_degrees:
					mean_number_occur=sum(cumulative_occurrence_per_degree_dict[udeg])/len(cumulative_occurrence_per_degree_dict[udeg])

					average_occurrence_per_degree_dict[udeg]=mean_number_occur

				#3. time until first occurrence
				#eg. "for nodes of degree 32, time until first occurrence was t_0 timesteps"

				cumulative_time_until_occurrence_per_degree_dict={}

				for u_deg in unique_degrees:
					cumulative_time_until_occurrence_per_degree_dict[u_deg]=[]

				for name in unique_names:

					time_until_first=degree_time_until_first_dict[name]
					corresponding_degree=degree_name_dict[name]

					cumulative_time_until_occurrence_per_degree_dict[corresponding_degree].append(time_until_first)

				mean_time_until_occurrence_per_degree_dict={}

				for udeg in unique_degrees:

					#rip out not found ones...

					list_in_question=cumulative_time_until_occurrence_per_degree_dict[udeg]

					remaining_list=[l for l in list_in_question if l!='NOT_FOUND']

					if len(remaining_list)>0:
						mean_time_until_occur=sum(remaining_list)/len(remaining_list)
					else:
						mean_time_until_occur='NEVER_HAPPENS'

					mean_time_until_occurrence_per_degree_dict[udeg]=mean_time_until_occur

				#										RESULT VECTORS									#			
				#=======================================================================================#
				# mean_time_until_occurrence_per_degree_dict	(avg time until it occurs)
				# average_occurrence_per_degree_dict			(number of occurrences)
				# cumulative_degree_has_occured_dict_avg		(it has or hasn't occurred as percentage)


				#										SAVING									#			
				#===============================================================================#

				#mean time until
				mtu_out_str=return_save_fn('time_until_occur',working_directory,NUM_PARTICLES,COINCIDENT_EXPOSURE_THRESHOLD,results_dir)
				#average occurrence per degree
				avg_occur_str=return_save_fn('avg_occur_deg',working_directory,NUM_PARTICLES,COINCIDENT_EXPOSURE_THRESHOLD,results_dir)
				#percentage_occurence(0,1) per degree
				perc_occur_str=return_save_fn('perc_occur_deg',working_directory,NUM_PARTICLES,COINCIDENT_EXPOSURE_THRESHOLD,results_dir)



				pickle_save_obj(mean_time_until_occurrence_per_degree_dict,mtu_out_str)
				pickle_save_obj(average_occurrence_per_degree_dict,avg_occur_str)
				pickle_save_obj(cumulative_degree_has_occured_dict_avg,perc_occur_str)

				
				print('Round Completed for: ' + str(BA_GRAPH_NUMBER))

			#save outputs...