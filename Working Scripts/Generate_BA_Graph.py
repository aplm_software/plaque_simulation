#reqs import

from reqs import *

def Generate_BA_Graph(graph_no,BA_SIZE):
	#create new BA
	cur_BA=BAG_RAW.bag(BA_SIZE,2)
	save_dir='results/BA_Graph_' + str(graph_no) + '/'

	#overwrite if exists.
	if os.path.isdir(save_dir):
		shutil.rmtree(save_dir)

	#and then make a new directory
	os.mkdir(save_dir)

	#save it
	bag_fn='BA_Graph_' + str(graph_no) + '.txt'
	out_file = open(save_dir + bag_fn, 'w+')
	cPickle.dump(cur_BA, out_file)
	out_file.close()	
